<?php

class Users extends CI_Controller
{
	
	// User Sign up
	public function register()
	{
		$data['title'] = 'Sign Up';

		$this->form_validation->set_rules('username','Username','required|callback_check_username_exists');
		$this->form_validation->set_rules('email','Email','required|callback_check_email_exists');
		$this->form_validation->set_rules('password','Password','required');


		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('users/register', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$enc_password = md5($this->input->post('password'));
			$this->user_model->register($enc_password);

			$this->session->set_flashdata('user_registered', 'Registration completed successfully');

			redirect('posts');
		}
	}

	// User Login
	public function login()
	{
		$data['title'] = 'Sign In';

		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');


		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('users/login', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$username = $this->input->post('username');
			$password = md5($this->input->post('password'));
			$user_id = $this->user_model->login($username,$password);

			if($user_id){
				// session creation
				$user_data = array(
					'user_id' => $user_id->id,
					'username' => $username,
					'login_state' => true
				);
				$this->session->set_userdata($user_data);

				$this->session->set_flashdata('user_loggedin', 'User Logged in');
				redirect('posts');

			}else{
				$this->session->set_flashdata('login_failed', 'User Login Failed');
				redirect('users/login');
			}
		}
	}
	// user log out
	public function logout()
	{
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('login_state');
		redirect('users/login');

	}

	public function check_username_exists($username)
	{
		$this->form_validation->set_message('check_username_exists', 'Username exists, please try another');
		if($this->user_model->check_username_exists($username)){
			return true;
		}else{
			return false;
		}
	}
	public function check_email_exists($email)
	{
		$this->form_validation->set_message('check_email_exists', 'Email exists, please try another');
		if($this->user_model->check_email_exists($email)){
			return true;
		}else{
			return false;
		}
	}
}

?>