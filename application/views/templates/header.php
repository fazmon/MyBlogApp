<html>
<head>
	<title>My Blog</title>
	<!-- <link rel="stylesheet" href="https://bootswatch.com/4/flatly/bootstrap.min.css"> -->
  <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="/assets/css/styles.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="/">MyBlog</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/about">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/posts">Blog</a>
      </li>
    </ul>

    <?php if($this->session->userdata('login_state')) : ?>
      <div class="nav pull-right">
        <a class="nav-link" href="/posts/create">Create Post</a>
      </div>
      <div class="nav pull-right">
        <a class="nav-link" href="/users/logout">Logout</a>
      </div>
    <?php endif; ?>

    <?php if(!$this->session->userdata('login_state')) : ?>
      <div class="nav pull-right">
        <a class="nav-link" href="/users/register">Register</a>
      </div>
      <div class="nav pull-right">
        <a class="nav-link" href="/users/login">Login</a>
      </div>
   <?php endif; ?>
  </div>
</nav>

<div class="container">
    <br>
    <?php if($this->session->flashdata('user_registered')): ?>
    <?php  echo '<p class="alert alert-success">'.$this->session->flashdata('user_registered').'</p>' ?>
    <?php endif; ?>

    <?php if($this->session->flashdata('user_loggedin')): ?>
    <?php  echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedin').'</p>' ?>
    <?php endif; ?>

    <?php if($this->session->flashdata('login_failed')): ?>
    <?php  echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>' ?>
    <?php endif; ?>