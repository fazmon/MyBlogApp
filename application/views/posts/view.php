<h4><?php echo $post['title']; ?> </h4>
<small class="post-date"> Posted On: <?php echo $post['created_at']; ?></small><br/>
<img src="<?php echo site_url();?>assets/images/posts/<?php echo $post['post_image'] ?>" style="width: 170px;" >
<div class="post-body">
	<?php echo $post['description']; ?>
</div>

<hr>
<?php if($this->session->userdata('user_id') == $post['user_id'] ) : ?>
  <a class="btn btn-default" href="/posts/edit/<?php echo $post['slug']; ?>">Edit</a>
  <?php echo form_open('posts/delete/'.$post['id']); ?>
  <input type="submit" value="Delete" class="btn btn-danger"> 
</form>
<?php endif; ?>
<hr>
<h3>Comments</h3>
<?php if($comments) :  ?>
<?php foreach ($comments as $comment): ?>
	<div class="well">
	<?php echo $comment['body']; ?> by [ <?php echo $comment['name'] ?>] <br>
	</div>
<?php endforeach;  ?>

<?php else :  ?>
	<h5>No comments yet!!!</h5>
<?php endif;  ?>


<hr>
<div class="col-md-6 float-right"> 
<?php if($this->session->userdata('login_state')) : ?>
<?php echo form_open('comments/create/'.$post['id']); ?>
<div class="form-group">
   <!--  <label>Name</label> -->
    <input type="text" class="form-control" name="name" placeholder="Enter name">
  </div>

  <div class="form-group">
   <!--  <label>Email</label> -->
    <input type="text" class="form-control" name="email" placeholder="Enter email">
  </div>

<div class="form-group">
    <label>Body</label>
    <textarea  class="form-control" name="body" placeholder="Enter body"> </textarea>
 </div>

 <input type="hidden" name="slug" value="<?php echo $post['slug']; ?>">
 <button type="submit" class="btn btn-primary">Submit</button>
</form> 
<?php endif; ?>
</div>