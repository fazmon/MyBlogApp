<br>
<?php foreach ($posts as $post) : ?>
	<h4> <?php echo $post['title']; ?></h4>
	<div class="row">
		<div class="col-md-3">
			<img src="<?php echo site_url();?>assets/images/posts/<?php echo $post['post_image'] ?>" style="width: 170px;" >
		</div>
		<div class="col-md-9">
			<small class="post-date"> Posted On: <?php echo $post['created_at']; ?> in <?php echo $post['name']; ?> Category </small><br/>
			<?php echo word_limiter($post['description'],50); ?>
			<p> <a class="btn btn-primary" href="<?php echo site_url('/posts/'.$post['slug']); ?>">Read More </a></p>
		</div> 
	</div>
<?php endforeach; ?>



	

<div class="pagination-links text-center">
	<?php echo $this->pagination->create_links(); ?>
</div>



