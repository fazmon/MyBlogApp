<h4><?= $title; ?></h4>

<?php echo validation_errors(); ?>

<?php echo form_open_multipart('posts/create'); ?>
  <div class="form-group">
    <label>Title</label>
    <input type="text" class="form-control" name="title" placeholder="Enter titile">
  </div>

  <div class="form-group">
    <label>Category</label>
    <select  class="form-control" name="category_id" >
    	<?php foreach ($categories as $category) : ?> 
    	<option value="<?php echo $category['id'];  ?>" > <?php echo $category['name']; ?> </option>
    	<?php endforeach; ?>

    </select>
  </div>

  <div class="form-group">
    <label>Description</label>
    <textarea  class="form-control" name="description" placeholder="Enter description"> </textarea>
  </div>

  <div class="form-group">
    <label>Upload image</label>
    <input type="file" name="postimage" class="form-control" size="20">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form> 