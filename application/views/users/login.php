<?php echo form_open('users/login'); ?>
<div class="row">
  <div class="col-md-4 col-md-offset-4">
    <h3 class="text-center"><?= $title; ?></h3>
    <div class="form-group">
      <input type="text" class="form-control" name="username" placeholder="Enter Username">
    </div>

    <div class="form-group">
      <input type="password" class="form-control" name="password" placeholder="Enter Password">
    </div>

    <button type="submit" class="btn btn-primary btn-block">Login</button>
  </div>
</div>
<?php echo form_close(); ?>